#!/bin/bash

cd "$(dirname \"`readlink -f ${BASH_SOURCE[0]}`\")"

target="lgm"

make TARGET="$target"
sudo make install TARGET="$target"

echo "NOTE: Tab completion for directories is available"
echo -n "Enter your base project directory(default is $HOME/Downloads): "
read -e base_dir_name
if [ -n "$base_dir_name" ] && [ -d "$base_dir_name" ]; then
    config_str="# Config for lgm\nalias '$target'='$target -b \"$base_dir_name\"'"
    echo -e $config_str >> ~/.bashrc
fi
