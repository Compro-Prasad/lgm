PREFIX = /usr/local
INSTALL_PREFIX = $(PREFIX)/bin
TARGET = lgm

$(TARGET): lgm.py
	cp $< $@
	chmod +x $@

.PHONY: clean install
clean:
	-rm -f $(TARGET)
install: $(TARGET)
	-cp -f $(TARGET) $(INSTALL_PREFIX)
