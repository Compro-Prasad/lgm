#!/usr/bin/python3

import argparse
import sys
import os


# parser = argparse.ArgumentParser(description='Process some integers')
# parser.add_argument('integers', metavar='N', type=int, nargs='+',
#                     help='an integer for the accumulator')
# parser.add_argument('--sum', dest='accumulate', action='store_const',
#                     const=sum, default=max,
#                     help='sum the integers (default: find the max)')
# args = parser.parse_args()
# print(args)
# print(type(args))
# print(args.accumulate(args.integers))

SUCCESS = 0
dir_path = None
remote_url = None
symlink_list = None


def parseCmdline():
    '''Read the command line arguments and modify the variables'''
    global remote_url
    global dir_path
    global symlink_list

    parser = argparse.ArgumentParser(
        description='Maintain different git repositories locally'
    )

    parser.add_argument(
        'url',
        type=str,
        help='URL of the remote repository'
    )

    parser.add_argument(
        'path',
        type=str,
        nargs="*",
        help='Symlink name to the actual repo'
    )

    parser.add_argument(
        '-b',
        '--base-dir',
        type=str,
        default=os.getenv('HOME') + '/Downloads',
        help='To specify the projects directory'
    )

    arg_values = parser.parse_args()

    remote_url = arg_values.url
    symlink_list = arg_values.path
    dir_path = arg_values.base_dir
    # strip trailing slashes('/')
    if dir_path[-1] == "/":
        i = -1
        while dir_path[i] == "/":
            i -= 1
        dir_path = dir_path[:i]


def correct_url(url=remote_url):
    '''Prefix the URL with a 'git://' if no protocol is explicitly provided'''
    if url.startswith('//'):
        return 'git:' + url
    elif url.startswith('/'):
        return 'git:/' + url
    elif not (url.startswith('git://') or url.startswith('http')):
        return 'git://' + url
    return url


def urlIsValid(url=remote_url):
    '''Checks the URL if it is valid'''
    return (os.system("git ls-remote '" + url + "' &>-") == SUCCESS)


def git_remote_url_split(url=remote_url):
    '''Given an URL split it into domain, user and repo'''
    tmp = url.split('://')[1].split('/')

    # Remove .git extension from repo
    repo = tmp[2].split('.')
    if repo[-1] == 'git':
        repo = '.'.join(repo[:-1])
    else:
        repo = '.'.join(repo)
    return {
        'domain': tmp[0],
        'user': tmp[1],
        'repo': repo,
    }


def generate_dir_path(old_dir_path, url=remote_url):
    details = git_remote_url_split(url)
    new_dir_path = old_dir_path + '/' + details['domain']
    new_dir_path += '/' + details['user']
    new_dir_path += '/' + details['repo']
    return new_dir_path


if __name__ == '__main__':
    parseCmdline()
    remote_url = correct_url(remote_url)
    if urlIsValid(remote_url):
        return_value = 0
        clone_flag = False
        print("Git repository is valid")
        dir_path = generate_dir_path(dir_path, remote_url)
        if os.path.exists(dir_path):
            print("The location '" + dir_path + "' already exists")
            remove = input("Remove file/directory? ").strip()[0].upper()
            if remove == 'Y':
                from shutil import rmtree
                rmtree(dir_path)
                clone_flag = True
            elif len(symlink_list) == 0:
                details = git_remote_url_split(remote_url)
                repo_location = input("Enter repository location[default=./" + details['repo'] + "]: ").strip()
                if repo_location == "":
                    symlink_list.append(details['repo'])
                else:
                    symlink_list.append(repo_location)
        else:
            clone_flag = True
        if clone_flag:
            cmd = "git clone '" + remote_url + "' '" + dir_path + "'"
            print("Running : " + cmd)
            return_value = os.system(cmd)
        if return_value == 0:
            for i in symlink_list:
                print("Making symlink to", i)
                os.symlink(dir_path, i)
            # Not to make it optional. 90% would like to cd into the repository.
            # So why not provide them a separate shell session to isolate the work
            print("Exit the following shell session to get back to work")
            os.system("cd '" + dir_path + "' && /bin/bash")
        exit(return_value)

    else:
        print("Error: Not a git repo")
        os._exit(-1)
